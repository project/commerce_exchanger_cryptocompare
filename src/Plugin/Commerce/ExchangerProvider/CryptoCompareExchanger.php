<?php

namespace Drupal\commerce_exchanger_cryptocompare\Plugin\Commerce\ExchangerProvider;

use Drupal\commerce_exchanger\Plugin\Commerce\ExchangerProvider\ExchangerProviderRemoteBase;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Http\ClientFactory;
use Psr\Log\LoggerInterface;

/**
 * Provides CryptoCompare Rates.
 *
 * @CommerceExchangerProvider(
 *   id = "crypto_compare",
 *   label = "CryptoCompare",
 *   display_label = "CryptoCompare",
 *   historical_rates = FALSE,
 *   enterprise = TRUE,
 *   refresh_once = FALSE,
 *   api_key= TRUE,
 * )
 */
class CryptoCompareExchanger extends ExchangerProviderRemoteBase {

  /**
   * The exchange rates storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $exchangeRatesStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, ClientFactory $http_client_factory, ConfigFactory $config_factory, LoggerInterface $logger_channel) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $http_client_factory, $config_factory, $logger_channel);

    $this->exchangeRatesStorage = $entity_type_manager->getStorage('commerce_exchange_rates');
  }

  /**
   * {@inheritdoc}
   */
  public function apiUrl() {
    return 'https://min-api.cryptocompare.com/data/pricemulti';
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteData($base_currency = NULL) {
    $data = NULL;

    $currencies = \Drupal::service('entity_type.manager')->getStorage('commerce_currency')->loadMultiple();

    $exchange_rate_entity = $this->exchangeRatesStorage->loadByProperties([
      'plugin' => $this->getPluginDefinition()['id'],
    ]);
    $exchange_rate_entity = reset($exchange_rate_entity);
    $currency_code_mapping = $this->currencyCodeMapping($exchange_rate_entity->getThirdPartySetting('commerce_exchanger_cryptocompare', 'currency_code_mapping', ''));

    // Prepare client.
    $options = [
      'query' => [
        'tsyms' => str_replace($currency_code_mapping['search'], $currency_code_mapping['replace'], implode(',', array_keys($currencies))),
        'api_key' => $this->getApiKey(),
      ],
    ];

    if (!empty($base_currency)) {
      $options['query']['fsyms'] = str_replace($currency_code_mapping['search'], $currency_code_mapping['replace'], $base_currency);
    }
    else {
      $options['query']['fsyms'] = str_replace($currency_code_mapping['search'], $currency_code_mapping['replace'], implode(',', array_keys($currencies)));
      $base_currency = array_keys($currencies)[0];
    }

    $response = $this->apiClient($options);

    if ($response) {
      $exchange_rates = Json::decode($response);

      $data['base'] = $base_currency;
      $data['rates'] = [];

      foreach ($exchange_rates[str_replace($currency_code_mapping['search'], $currency_code_mapping['replace'], $base_currency)] as $currency => $exchange_rate) {
        $data['rates'][str_replace($currency_code_mapping['replace'], $currency_code_mapping['search'], $currency)] = $exchange_rate;
      }
    }

    return $data;
  }

  /**
   * Parse the currency_code_mapping config.
   *
   * @param string $config
   *   Raw config.
   *
   * @return array
   *   Mapped array for str_replace().
   */
  protected function currencyCodeMapping(string $config): array {
    $currency_code_mapping_parsed = explode(PHP_EOL, $config);
    $currency_code_mapping = [
      'search' => [],
      'replace' => [],
    ];

    foreach ($currency_code_mapping_parsed as $mapping) {
      if (strlen($mapping) > 0) {
        $mapping = explode('|', trim($mapping));

        $currency_code_mapping['search'][] = $mapping[0];
        $currency_code_mapping['replace'][] = $mapping[1];
      }
    }

    return $currency_code_mapping;
  }

}
