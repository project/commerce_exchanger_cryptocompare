<?php

namespace Drupal\Tests\commerce_exchanger_cryptocompare\FunctionalJavascript;

use Drupal\Tests\commerce\FunctionalJavascript\CommerceWebDriverTestBase;

/**
 * Functional JS tests to ensure modules functionality.
 *
 * @group commerce_exchanger_cryptocompare
 */
class CryptoCompareExchangerUiTest extends CommerceWebDriverTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'commerce_exchanger_cryptocompare',
    'commerce_exchanger_cryptocompare_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function getAdministratorPermissions() {
    return array_merge([
      'administer commerce exchanger settings',
    ], parent::getAdministratorPermissions());
  }

  /**
   * Test Exchanger creation in UI.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testExchangerCreate() {
    $this->drupalGet('admin/commerce/config/exchange-rates');
    $this->getSession()->getPage()->clickLink('Add Exchange rates');

    $this->getSession()->getPage()->fillField('label', 'Crypto Compare');
    $this->getSession()->getPage()->selectFieldOption('plugin', 'ecb');
    $this->assertSession()->assertWaitOnAjaxRequest();

    $this->getSession()->getPage()->selectFieldOption('plugin', 'crypto_compare');
    $this->assertSession()->assertWaitOnAjaxRequest();

    $this->getSession()->getPage()->pressButton('Edit');
    $this->getSession()->getPage()->fillField('id', 'crypto_compare');

    $this->getSession()->getPage()->fillField('configuration[crypto_compare][api_key]', 'APIKEY');
    $this->getSession()->getPage()->fillField('configuration[crypto_compare][enterprise]', TRUE);

    $this->getSession()->getPage()->pressButton('Save');

    $this->assertSession()->pageTextContains(t('Saved the Crypto Compare exchange rates.'));
  }

}
