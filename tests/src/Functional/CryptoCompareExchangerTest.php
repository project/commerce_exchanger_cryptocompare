<?php

namespace Drupal\Tests\commerce_exchanger_cryptocompare\Functional;

use Drupal\Tests\commerce\Functional\CommerceBrowserTestBase;

/**
 * Functional tests to ensure modules functionality.
 *
 * @group commerce_exchanger_cryptocompare
 */
class CryptoCompareExchangerTest extends CommerceBrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'commerce_exchanger_cryptocompare',
    'commerce_exchanger_cryptocompare_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function getAdministratorPermissions() {
    return array_merge([
      'administer commerce exchanger settings',
    ], parent::getAdministratorPermissions());
  }

  /**
   * Test if the imported exchanger from config exists.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testExchangerLoadFromConfig() {
    $this->drupalGet('admin/commerce/config/exchange-rates');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->pageTextContains('CryptoCompare');
    $this->assertSession()->pageTextContains('cryptocompare');
    $this->assertSession()->pageTextContains('live');
    $this->assertSession()->pageTextContains('Enabled');
  }

}
