<?php

namespace Drupal\Tests\commerce_exchanger_cryptocompare\Kernel;

use Drupal\commerce_exchanger\Entity\ExchangeRates;
use Drupal\commerce_price\Entity\Currency;
use Drupal\commerce_price\Price;
use Drupal\Tests\commerce\Kernel\CommerceKernelTestBase;

/**
 * Kernel tests to ensure modules functionality.
 *
 * @group commerce_exchanger_cryptocompare
 */
class CurrencyCodeMappingTest extends CommerceKernelTestBase {

  /**
   * Price in BTC currency.
   *
   * @var \Drupal\commerce_price\Price
   */
  protected Price $priceCAKE;

  /**
   * Price in ETH currency.
   *
   * @var \Drupal\commerce_price\Price
   */
  protected Price $priceUSDC;

  /**
   * Imported exchange rates for BTC.
   *
   * @var array
   */
  protected array $ratesCAKE;

  /**
   * Imported exchange rates for ETH.
   *
   * @var array
   */
  protected array $ratesUSDC;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'commerce_exchanger',
    'commerce_exchanger_cryptocompare',
    'commerce_exchanger_cryptocompare_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['commerce_exchanger']);
    $this->installConfig(['commerce_exchanger_cryptocompare_test']);

    // Add third party setting currency_code_mapping.
    $crypto_compare_exchanger = $this->entityTypeManager->getStorage('commerce_exchange_rates')->load('cryptocompare');
    $crypto_compare_exchanger->setThirdPartySetting('commerce_exchanger_cryptocompare', 'currency_code_mapping', 'USC|USDC
CAK|CAKE')
      ->save();

    // Add CAKE and TRTL currencies.
    $cake = Currency::create([
      'currencyCode' => 'CAK',
      'name' => 'PancakeSwap',
      'numericCode' => '000',
      'symbol' => 'CAKE',
      'fractionDigits' => 18,
    ]);
    $cake->save();

    $usd_coin = Currency::create([
      'currencyCode' => 'USC',
      'name' => 'USD Coin',
      'numericCode' => '000',
      'symbol' => 'USDC',
      'fractionDigits' => 6,
    ]);
    $usd_coin->save();

    $crypto_compare_exchange_rates = ExchangeRates::load('cryptocompare')->getPlugin();
    $crypto_compare_exchange_rates->import();

    $this->ratesCAKE = $crypto_compare_exchange_rates->getRemoteData('CAK');
    $this->ratesUSDC = $crypto_compare_exchange_rates->getRemoteData('USC');

    $this->priceCAKE = new Price('1', 'CAK');
    $this->priceUSDC = new Price('1', 'USC');
  }

  /**
   * Test exchanger plugin import functionality.
   */
  public function testExchangerRatePluginImport() {
    $exchange_rates = $this->container->get('commerce_exchanger.calculate')->getExchangeRates();

    $this->assertArrayHasKey('CAK', $exchange_rates, 'PancakeSwap rates not available');
    $this->assertNotNull($exchange_rates["USD"]["CAK"]["value"], 'No PancakeSwap - USD rate imported.');

    $this->assertArrayHasKey('USC', $exchange_rates, 'USD Coin rates not available');
    $this->assertNotNull($exchange_rates["USD"]["USC"]["value"], 'No USD Coin - USD rate imported.');
  }

  /**
   * Test actual BTC price conversion.
   */
  public function testPancakeSwapExchangerRatesPluginRemoteData() {
    $this->assertArrayHasKey('rates', $this->ratesCAKE, 'No PancakeSwap rates present');
    $this->assertArrayHasKey('CAK', $this->ratesCAKE['rates'], 'No PancakeSwap/USD rate present');

    $priceUsd = $this->ratesCAKE['rates']['USD'];
    $this->assertNotNull($priceUsd, 'No PancakeSwap - USD rate imported.');
    $this->assertEqualsWithDelta(4, $priceUsd, 4.0, 'Either test failed or PancakeSwap is up/down by a lot!');
  }

  /**
   * Test actual ETH price conversion.
   */
  public function testUsdcExchangerRatesPluginRemoteData() {
    $this->assertArrayHasKey('rates', $this->ratesUSDC, 'No USD Coin rates present');
    $this->assertArrayHasKey('USD', $this->ratesUSDC['rates'], 'No USD Coin/USD rate present');

    $priceUsd = $this->ratesUSDC['rates']['USD'];
    $this->assertNotNull($priceUsd, 'No USD Coin - USD rate imported.');
    $this->assertEqualsWithDelta(1, $priceUsd, 0.2, 'Either test failed or USD Coin is up/down by a lot!');
  }

}
