<?php

namespace Drupal\Tests\commerce_exchanger_cryptocompare\Kernel;

use Drupal\commerce_exchanger\Entity\ExchangeRates;
use Drupal\commerce_price\Price;
use Drupal\Tests\commerce\Kernel\CommerceKernelTestBase;

/**
 * Kernel tests to ensure modules functionality.
 *
 * @group commerce_exchanger_cryptocompare
 */
class CryptoCompareExchangerRatesImportTest extends CommerceKernelTestBase {

  /**
   * Price in BTC currency.
   *
   * @var \Drupal\commerce_price\Price
   */
  protected $priceBtc;

  /**
   * Price in ETH currency.
   *
   * @var \Drupal\commerce_price\Price
   */
  protected $priceEth;

  /**
   * Imported exchange rates for BTC.
   *
   * @var array
   */
  protected $ratesBtc;

  /**
   * Imported exchange rates for ETH.
   *
   * @var array
   */
  protected $ratesEth;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'commerce_exchanger',
    'commerce_exchanger_cryptocompare',
    'commerce_exchanger_cryptocompare_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['commerce_exchanger']);
    $this->installConfig(['commerce_exchanger_cryptocompare_test']);

    $crypto_compare_exchange_rates = ExchangeRates::load('cryptocompare')->getPlugin();
    $crypto_compare_exchange_rates->import();

    $this->ratesBtc = $crypto_compare_exchange_rates->getRemoteData('BTC');
    $this->ratesEth = $crypto_compare_exchange_rates->getRemoteData('ETH');

    $this->priceBtc = new Price('1', 'BTC');
    $this->priceEth = new Price('1', 'ETH');
  }

  /**
   * Test exchanger plugin import functionality.
   */
  public function testExchangerRatePluginImport() {
    $exchange_rates = $this->container->get('commerce_exchanger.calculate')->getExchangeRates();

    $this->assertArrayHasKey('BTC', $exchange_rates, 'BTC rates not available');
    $this->assertNotNull($exchange_rates["USD"]["BTC"]["value"], 'No BTC - USD rate imported.');

    $this->assertArrayHasKey('ETH', $exchange_rates, 'ETH rates not available');
    $this->assertNotNull($exchange_rates["USD"]["ETH"]["value"], 'No BTC - USD rate imported.');
  }

  /**
   * Test actual BTC price conversion.
   */
  public function testBtcExchangerRatesPluginRemoteData() {
    $this->assertArrayHasKey('rates', $this->ratesBtc, 'No BTC rates present');
    $this->assertArrayHasKey('USD', $this->ratesBtc['rates'], 'No BTC/USD rate present');

    $priceUsd = $this->ratesBtc['rates']['USD'];
    $this->assertNotNull($priceUsd, 'No BTC - USD rate imported.');
    $this->assertEqualsWithDelta(30000.0, $priceUsd, 20000.0, 'Either test failed or BTC is up/down by a lot!');
  }

  /**
   * Test actual ETH price conversion.
   */
  public function testEthExchangerRatesPluginRemoteData() {
    $this->assertArrayHasKey('rates', $this->ratesEth, 'No ETH rates present');
    $this->assertArrayHasKey('USD', $this->ratesEth['rates'], 'No ETH/USD rate present');

    $priceUsd = $this->ratesEth['rates']['USD'];
    $this->assertNotNull($priceUsd, 'No ETH - USD rate imported.');
    $this->assertEqualsWithDelta(1500.0, $priceUsd, 1500.0, 'Either test failed or ETH is up/down by a lot!');
  }

}
