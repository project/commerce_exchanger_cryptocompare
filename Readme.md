# Commerce Exchanger CryptoCompare

## Contents of this file

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

## Introduction

Provides a commerce exchanger plugin to fetch exchange rates from the CryptoCompare API.

<img src="https://www.drupal.org/files/project-images/commerce_exchanger_cryptocompare_1.png" />

## Requirements

Requires [Commerce Exchanger](https://www.drupal.org/project/commerce_exchanger).

## Installation

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/node/1897420 for further information.

## Configuration

<ul>
<li>Create some Crypto Currencies under Commerce / Configuration / Currencies.
</li>
<li>You will need a API Key from https://min-api.cryptocompare.com to make
this work.</li>
<li>Go to Commerce / Configuration / Exchange rates and add a new Exchange Rate.
</li>
<li>Choose "CryptoCompare", fill in your API Key and save.</li>
<li>Trigger Cron, the rates will be fetched when Cron runs.</li>
</ul>

## Maintainers

Current maintainer:
 * David Bätge (daveiano) - https://www.drupal.org/u/daveiano
